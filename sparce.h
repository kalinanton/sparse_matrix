#pragma once

#include "pch.h"
#include <algorithm>

template<template<class T> class Container> class static_matrix_portrait : protected Container<std::pair<int,int>> {
private:
	bool is_complete;

	struct Cache {
		struct Row_Data {
			int row_size;
			int row_pos;
		};

		Container<Row_Data> row_data;
	};

	std::unique_ptr<Cache> cache_ptr;

public:
	template<class _T> using Container_Type = Container<_T>;

	static_matrix_portrait() : is_complete(false), cache_ptr(new Cache) {};

	void operator() (int i, int j) {
		if (is_complete) is_complete = false;
		this->push_back(std::pair<int, int>(i, j));
	}

	void make_complete() {
		if (is_complete) return;
		std::sort(this->begin(), this->end(), [](auto & f, auto & s) -> bool { return f.first == s.first ? f.second < s.second : f.first < s.first; });

		cache_ptr.reset(new Cache);
		typename Cache::Row_Data * rowdata = nullptr;
		for (int i = 0; i < this->size(); i++) {
			if (cache_ptr->row_data.size() <= (*this)[i].first) {
				cache_ptr->row_data.push_back(Cache::Row_Data());
				rowdata = &(cache_ptr->row_data[cache_ptr->row_data.size() - 1]);
			};
			rowdata->row_size++;
		}
	}

	void get_row(int i, Container<int> & row) const {
		if (!is_complete) throw std::exception("Incomplete matrix portrait");

		typename Cache::Row_Data * rowdata = &cache_ptr->row_data[i];
		row.resize(rowdata->row_size);
		for (int i = 0; i < rowdata->row_size; i++)
			row[i] = i + rowdata->row_size;
	}

	const size_t size() const { return Container<std::pair<int, int>>::size(); }
	const bool complete() const { return is_complete; }
};

template<template<class _T> class Container, class T> class static_sparse_matrix : protected Container<T> {
private:
	std::shared_ptr<static_matrix_portrait<Container>> portrait;

	struct Cache {
		Container<int> curr_row;
	};

	std::unique_ptr<Cache> cache_ptr;

public:
	typedef T value_type;

	static_sparse_matrix(std::shared_ptr<static_matrix_portrait<Container>>& _portrait) : portrait(_portrait), cache_ptr(new Cache) {}
};